export default function({ $axios, store }) {
  // onRequest是@nuxtjs/axios模塊提供的幫助方法
  $axios.onRequest(config => {
    // 附加令牌
    if (store.state.user.token) {
      config.headers.Authorization = "Bearer " + store.state.user.token;
    }
    return config;
  });
}
