import Vue from 'vue'
import {scrollTo} from '@/utils/scroll-to.client.js';

// 綁定到全局 vue。$data自己申明的全局變量
Vue.prototype.$scrollTo = scrollTo;
