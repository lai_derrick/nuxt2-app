export const state = () => ({
  token: "",
  name: "",
  email: "",
  avatar: "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
});

export const mutations = {
  setToken(state, token) {
    state.token = token;
    this.$cookies.set('token', token)
    // window.localStorage.setItem("token", token);
  },
  setInformation(state, information) {
    state.name = information.name;
    state.email = information.email;
  },
  dataReset(state) {
    this.$cookies.remove('token')
    // window.localStorage.removeItem("token", token);
    state.token = "";
    state.name = "";
    state.email = "";
  },
};

export const getters = {
  isLogin(state) {
    return !!state.token;
  },
  hasInformation(state) {
    return !!state.email && !!state.name;
  },
  nickName(state) {
    return state.name || state.email;
  },
  information(state) {
    return {
      name: state.name,
      email: state.email,
      avatar: state.avatar,
    };
  },
};

export const actions = {
  async login({ commit, getters, dispatch }, loginForm) {
    const data = await this.$axios.$post("/user/login", loginForm);
    commit("setToken", data.result.token);
    dispatch("information");
    return { isLogin: getters.isLogin, data };
  },
  async registe({ commit, getters, dispatch }, registeForm) {
    const data = await this.$axios.$post("/user/register", registeForm);
    commit("setToken", data.result.token);
    dispatch("information");
    return { isLogin: getters.isLogin, data };
  },
  async information({ commit }) {
    const { result } = await this.$axios.$get("/user/information");
    commit("setInformation", result);
  },
  async logout({ commit }) {
    await this.$axios.$post("/user/logout");
    commit("dataReset");
  },
  async updateInformation({ commit }, accountForm) {
    const { result } = await this.$axios.$put("/user", accountForm);
    commit("setInformation", result);
  },
};
