export default function ({ route, redirect, store }) {
    const isLogin = store.getters["user/isLogin"];
  // 通過vuex中令牌存在與否判斷是否登錄
  if (!isLogin) {
    redirect("/login?redirect=" + route.path);
  }
}
