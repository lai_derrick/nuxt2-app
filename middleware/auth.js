export default async ({ store }) => {
  const hasInformation = store.getters["user/hasInformation"];
  const isLogin = store.getters["user/isLogin"];
  if (isLogin) {
    if (!hasInformation) {
      try {
        await store.dispatch("user/information");
      } catch (error) {
        store.commit("user/dataReset");
      }
    }
  }
};
